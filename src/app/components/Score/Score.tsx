import React, { FC } from 'react'
import { observer } from "mobx-react-lite";
import { game } from '../../../common/GameState';
import './Score.css'

type ScoreType = {
    className : string, 
    round: boolean, 
    team: number
}

export const Score: FC<ScoreType> = observer(({className, round, team}) => {

    const {roundScore, team1, team2} = game;
    
    const getTeamScore = (teamID : number) => {
        let team = (teamID == 1 && team1 || teamID == 2 && team2);
        return team.Score
    }
    
    return (
        <div className={`score ${className ? className : ''}`}>
            {round && roundScore || team && getTeamScore(team) || 0}
        </div>
    )
})