import React, { FC } from 'react'
import { Score } from '../Score'
import { observer } from "mobx-react-lite";
import './Team.css'
import { game } from '../../../common/GameState';

export const Team: FC<{teamID : number, admin : boolean | undefined}> = observer(({teamID, admin}) => {

    const { roundNumber, team1, team2, changeMistake } = game;
    let team = (teamID == 1 && team1 || teamID == 2 && team2);

    const handleClick = () => {
        if (admin) {
            changeMistake(teamID)
        }
    }

    const renderOpenedMistakes = () => {
        let mistakesDiv : React.JSX.Element[] = [];
        for (let i = 0; i < team.Mistakes; i++) {
            mistakesDiv.push(
                <div 
                    key= {i} 
                    onClick = {() => handleClick()}
                    className="mistake openedMistake">
                        X
                </div>)
        }
        return mistakesDiv
    }
    
    const renderClosedMistakes = () => {
        let mistakesDiv : React.JSX.Element[] = []
        for (let i = 0; i < 3 - team.Mistakes; i++) {
            mistakesDiv.push(
                <div 
                    key = {i} 
                    onClick = {() => handleClick()}
                    className="mistake closedMistake">
                </div>)
        }
        return mistakesDiv;
    }

    return (
    <div className="team">
        <div className="round__number">{roundNumber}</div>
        <Score team={teamID} className="team__score"/>
        <div className="team__name">{team.Name}</div>
        <div className="mistakes__wrapper">
            {renderOpenedMistakes()}
            {renderClosedMistakes()}
        </div>
    </div>
    )
} )