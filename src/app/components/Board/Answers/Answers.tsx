import React, { FC } from 'react'
import { observer } from "mobx-react-lite";
import { game } from '../../../../common/GameState';
import { Answer } from '../../../../common/interfaces/interfaces';
import './Answers.css'

export const Answers: FC<{admin : boolean | undefined}> = observer( ({admin}) => {

    const {getGameInfo, answers, openAnswer, changeRoundScore, roundNumber} = game;

    const handleClick = (answer : Answer) => {
        if(admin) {
            let action = answer.IsOpened.toString();
            changeRoundScore(answer.Cost, action, roundNumber)
            openAnswer(answer)
            } 
        }

    return (
        <>
        {answers ?
        answers.map((answer : Answer, index : number) => 
            {if(answer.IsOpened == true) {
                return ( 
                <div 
                    onClick={() => handleClick(answer)} 
                    key={index} 
                    className={`answer answer__opened`}>
                        {answer.Text}
                        <span className='answer__cost'>{answer.Cost}</span>
                </div>)
            } else if (admin) {
                return (
                <div 
                    onClick={() => handleClick(answer)} 
                    key={index} 
                    className={`answer answer__closed answer__admin`}>
                        {`${index+1} ${answer.Text}`}    
                </div>)
                } else {
                    return (
                        <div 
                            onClick={() => handleClick(answer)} 
                            key={index} 
                            className={`answer answer__closed`}>
                                <div className="answer__closed__number"> {index+1} </div>   
                        </div>)
                }
            })
            :
            <></>
        }
        </>
    )
}) 