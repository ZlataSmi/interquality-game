import React, { FC } from 'react'
import { observer } from "mobx-react-lite";
import { game } from '../../../common/GameState'
import { Score } from '../Score'
import { Answers } from './Answers'
import './Board.css'

export const Board: FC<{admin : boolean | undefined}> = observer(({admin}) => {

    const {question} = game;

    return (
    <div className="main__board">
        <Score round className="round__score"/>
        <div className="question">{question}</div>
        <div className="answers__wrapper">
            <Answers admin={admin}/>
        </div>
    </div>
    )
})


