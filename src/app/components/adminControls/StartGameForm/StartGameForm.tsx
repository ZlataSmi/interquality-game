import React, { FC, useState } from 'react'
import { observer } from "mobx-react-lite";
import { game } from '../../../../common/GameState';
import { api } from '../../../../common/API';
import './StartGameForm.css'

export const StartGameForm: FC = observer(() => {

    interface GameList {
        Games: {
            Id: number,
            Team1: string,
            Team2: string
        }[]
    }
   
    const {getGameInfo} = game;
    const {newGame, getGamesList} = api

    const [gameList, setGameList] = useState<GameList>()
 
    if(!gameList) {
        getGamesList().then((e) => {
            setGameList(e)
        })
    }
    
    const renderGameList = () => {

        let gameListToRender : React.JSX.Element[] = []
        if (gameList) {
            gameList.Games.slice(-3).map((gameListItem, index) => {
                gameListToRender.push(
                    <li className="game-list__item" key={index}>
                        {`game ID: ${gameListItem.Id} Team 1 name: ${gameListItem.Team1} Team 2 name: ${gameListItem.Team2}`}
                    </li>
                )
            })
        } 
        return gameListToRender;
    }
    
    const handleClickResumeGame = (event) => {
        event.preventDefault()
        getGameInfo(+event.target.gameID.value)
    }

    const handleClickNewGame = (event) => {
        event.preventDefault() 
        let gameVersion : number = +event.target[0].value;
        let team1name : string = event.target[1].value;
        let team2name : string = event.target[2].value;
        
        newGame(team1name, team2name, gameVersion).then(response =>  {
            getGameInfo(response.Data.Id)
            })
    }

    return (
    <>
        {gameList ? 
        <><div className="initGame">
            <form className='newGameForm' onSubmit={handleClickNewGame}>
            <label><h2>Выбор версии игры:</h2></label>
                <select className='newGame__version' id="version" name="version" required>
                    <option>1</option>
                    <option>2</option>
                </select>
                <label>Team 1 name:</label>
                <input id="team1name" type="text" name="team1" required/>
                <label>Team 2 name:</label>
                <input id="team2name" type="text" name="team2" required/>
                
                <button 
                    type="submit"
                    className='newGameBtn'>
                        Начать новую игру
                </button>
            </form>
            <div className="game-list">
                <h2>Список последних игр. Возобновить?</h2>
                <div className="listOfGames">{renderGameList()}</div>
                <form className='resumeGameForm' onSubmit={handleClickResumeGame}>
                    <label>Game ID:</label>
                    <input id="gameID" type="text" name="gameID" required/>
                    <button 
                        type="submit"
                        className='resumeGameBtn'>
                            Возобновить игру
                    </button>
                </form>
            </div>
        </div></>
        : 
        <div className="logo__noGame__wrapper">
            <img className='logo logo__noGame' src={'./img/game-logo.png'} alt="logo"/>
        </div>
        }
    </>
    )
})