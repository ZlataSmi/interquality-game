import React, { FC } from 'react'
import { observer } from "mobx-react-lite";
import { game } from '../../../../common/GameState';
import './NextRoundBtn.css'

export const NextRoundBtn: FC = observer(() => {

    const {nextRound} = game

    return (
        <button 
            className='nextRoundBtn'
            onClick={nextRound}>
                Next Round
        </button>
    )
})