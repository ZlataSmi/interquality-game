import React, { FC } from 'react'
import { observer } from "mobx-react-lite";
import './SendScoreToTeam.css'
import { game } from '../../../../common/GameState';

export const SendScoreToTeam: FC<{teamID : number}> = observer(({teamID}) => {

    const {changeTeamScore, roundScore} = game;
    const handleClick = () => {
        changeTeamScore(teamID, roundScore)
    }

    return (
        <button 
            className={`SendScoreToTeam ${teamID == 1 ? 'SendScoreToTeam1' : 'SendScoreToTeam2'}`}
            onClick={handleClick}>
                {teamID == 1 && 'очки команде 1 ←' || teamID == 2 && 'очки команде 2 →'}
        </button>
    )
})