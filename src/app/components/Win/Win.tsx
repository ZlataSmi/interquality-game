import React, { FC } from 'react'
import { game } from '../../../common/GameState';
import './Win.css'

export const Win: FC = () => {

    const {team1, team2} = game;

    const determineTheWinner = () => {     
        
        switch (true) {
            case team1.Score > team2.Score:
                return `Команда "${team1.Name}" выиграла!`
            case team1.Score < team2.Score:
                return `Команда "${team2.Name}" выиграла!`            
            case team1.Score == team2.Score:
                return `Ничья!`
            default:
              break;
        }
    }

    return (
        <div className="win__wrapper">
            <div className="win__content">
                <img className='logo logo__winGame' src={'./img/game-logo.png'} alt="logo"/>
                <p> {determineTheWinner()} </p>
                <p>Поздравляем!</p>
            </div>
        </div>
    )
}