import React, { FC } from 'react'
import { Team } from './components/Team'
import { Board } from './components/Board'
import { observer } from "mobx-react-lite"
import { game } from '../common/GameState'
import { api } from '../common/API'
import { Win } from './components/Win'
import './App.css'

let waitStarted = false;

export const App: FC = observer(() => {

    const {gameID, getGameInfo, roundNumber, changeRoundScore} = game;
    let {getLastGameID, getGameRevision } = api;

    const setRoundScore = (answers) => {
        let roundScore = 0;
        answers.map((e) => {
            if(e.IsOpened) {
                roundScore += e.Cost
            }           
        })
        changeRoundScore(roundScore, 'change', game.roundNumber)
    }

    function listenGameRevision (id) {
        getGameRevision(id).then((e) => { 

            if (game.revisionCount != e) {
                getGameInfo(id).then(() => {
                    setRoundScore(game.answers)               
                })
            }  
        })      
    }

    function waitForActiveGame() {
        getLastGameID()
        .then((id) => {
            if(id === null) {
                setTimeout(waitForActiveGame, 1000);
            } else {
                getGameInfo(id).then(() => setInterval(() => listenGameRevision(id), 1000))
            }
        })
    }

    if (gameID == null && !waitStarted ) {
        waitStarted = true;
        waitForActiveGame()
    }

    return (
    <>
        { gameID != null ? 
        <>
        <div className="app">
            <img className='logo logo__board' src={'./img/game-logo.png'} alt="logo"/>
            <div className="game">
                <Team teamID={1}/>
                <Board/>
                <Team teamID={2}/>
            </div>
            {roundNumber == 5 && <Win/>}
        </div>
        </>
        : 
        <div className="logo__noGame__wrapper">
            <img className='logo logo__noGame' src={'./img/game-logo.png'} alt="logo"/>
        </div>
        }
    </>
    )
})