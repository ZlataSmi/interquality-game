export interface Answer {
    Id: number, 
    IsOpened: boolean, 
    Text: string,
    Cost: number,
    Num: number
}

export interface Answers extends Array<Answer> {};

export interface Team {
    Id : number,
    Name: string,
    Score: number,
    Mistakes: number
}

export interface Teams extends Array<Team> {};