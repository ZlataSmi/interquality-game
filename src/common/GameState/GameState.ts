import { makeAutoObservable, runInAction } from "mobx";
import { Answers, Team, Answer } from "../interfaces/interfaces";
import { api, address } from "../API";

class GameState {

        gameID: number;
        revisionCount: number;
        roundNumber: number = 0;
        roundScore: number = 0;
        question : string;
        answers : Answers = [];
        team1: Team;
        team2: Team;
        win: boolean = false;
    
    constructor() {
        makeAutoObservable(this)
    }

    getGameInfo = async (id : number) => {

        let tmpResponse = await fetch(`${address}game?id=${id}`);
        let game = await tmpResponse.json()

        // for local server
        // let tmpResponse = await fetch('http://localhost:3001/game'); 
        // let game = await tmpResponse.json()
         
        runInAction(() => {

            this.gameID = id
            this.revisionCount = game.Data.Revision
            this.roundNumber = game.Data.Round
            this.question = game.Data.Question
            this.answers = game.Data.Answers
            this.team1 = game.Data.Team1
            this.team2 = game.Data.Team2
        })
   }

    openAnswer = (answer : Answer) => {
        api.openAnswer(this.gameID, answer.Num).then(() => {
            runInAction(() => {
                answer.IsOpened = !answer.IsOpened;
            })
        })
    }

    changeTeamScore = (team : number, changingValue : number ) => {
        
        let changingTeam : Team
        team == 1 ? changingTeam = this.team1 : changingTeam = this.team2
        api.sendScore(this.gameID, team, (changingTeam.Score + changingValue))
        .then(() => {
            
            runInAction(() => {
                changingTeam.Score += changingValue;
                this.roundScore = 0
            })
        })
    }

    changeMistake = (team : number) => {

        let changingTeam : Team
        team == 1 ? changingTeam = this.team1 : changingTeam = this.team2
        api.incrementMistakes(this.gameID, team).then(() => {
            runInAction (() => {
                changingTeam.Mistakes ++
            })
        }).then(() => {
            this.getGameInfo(this.gameID)
        })
    }

    nextRound = () => {
        if (this.roundNumber <= 4) {
            api.setNextRound(this.gameID)
            .then(() => {
                runInAction(()=> {
                    this.roundNumber ++
                    this.roundScore = 0
                })
            }).then(() => {
                this.getGameInfo(this.gameID)
            })
        } 
    }

    changeRoundScore = (changeValue : number, action : string, multiplier : number) => {

        if(multiplier == 4){
            multiplier = 1;
        }

        switch (action) {
            case 'true':
                this.roundScore = this.roundScore - changeValue * multiplier;
              break;
            case 'false':
                this.roundScore = this.roundScore + changeValue * multiplier;
              break;             
            case 'change':
                this.roundScore = changeValue * multiplier;
              break;
            default :
              break;
        }
    }
}

export const game = new GameState()