export const address = 'https://loki.bigbozo.games/100_to_1/api/'
// const address = 'http://192.168.1.166:8080'

class API {

    newGame = async (team1name : string, team2name : string, questionSet : number) => {
        let response = await fetch(`${address}/game?team1=${team1name}&team2=${team2name}&questionSet=${questionSet}`, {method : 'POST'}) 
        return await response.json()
    } 

    getGamesList = async () => {
        let response = await fetch(`${address}/listGames`);
        return await response.json();
    }

    getLastGameID = async () => {
        let response = await fetch(`${address}/lastGameId`);
        let lastgameID = await response.json()
        return lastgameID.id
    }

    getGameRevision = async (id : number) => {
        let tmpResponse = await fetch(`${address}/gameRevision?id=${id}`);
        let revision = await tmpResponse.json()
        return revision.revision
    }

    sendScore = async (gameID, teamID, teamScore) => {
       await fetch(`${address}/setScores?id=${gameID}&team=${teamID}&scores=${teamScore}`, {method : 'POST'}) 
    }

    setNextRound = async (nextRound) => {
        await fetch(`${address}/nextRound?id=${nextRound}`, {method : 'POST'})
    }

    incrementMistakes = async (gameID, teamID) => {
        await fetch(`${address}/incrementMistakes?id=${gameID}&team=${teamID}`, {method : 'POST'})
    }

    openAnswer = async (gameID, num) => {
        await fetch(`${address}/openAnswer?id=${gameID}&num=${num}`, {method : 'POST'})
    }
}

export const api = new API()