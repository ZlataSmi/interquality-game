import React from 'react'
import { Routes, Route } from 'react-router-dom'
import { App } from './app'
import { Admin } from './pages/admin'

export const AppRoutes = () => {
    return (
        <Routes>
            <Route index element={<App/>}></Route>
            <Route path='/admin' element={<Admin/>}></Route>
        </Routes>
    )
}
