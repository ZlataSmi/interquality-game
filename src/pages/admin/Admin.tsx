import React, { FC } from 'react'
import { observer } from "mobx-react-lite";
import { game } from '../../common/GameState';
import { Team } from '../../app/components/Team';
import { Board } from '../../app/components/Board';
import { NextRoundBtn } from '../../app/components/adminControls/NextRoundBtn';
import { SendScoreToTeam } from '../../app/components/adminControls/SendScoreToTeam';
import { StartGameForm } from '../../app/components/adminControls/StartGameForm';
import { Win } from '../../app/components/Win';

export const Admin: FC = observer(() => {

    const {revisionCount, roundNumber} = game;

    return (
    <>{revisionCount != undefined ? 
        <>
        <div className="admin app">
            <img className='logo logo__board' src={'./img/game-logo.png'} alt="logo"/>
            <div className="game">
                <Team admin teamID={1}/>
                <Board admin/>
                <Team admin teamID={2}/>
                <NextRoundBtn/>
                <SendScoreToTeam teamID={1}/>
                <SendScoreToTeam teamID={2}/>
            </div>
        </div>
        
        {roundNumber == 5 && <Win/>}
        </>
        : 
        <StartGameForm/>
    }</>
    )
}) 